# Module 2 - Gestion de projet

## Description Cours Magistral (CM)

⏱️ Durée : 2h

📑 Plan : 
- Comment bien gérer ses projets, organiser son espace de travail et ses scripts
- Cahier de labo
- Présentation des résultats aux biologistes
  - Gitlab Pages
  - Rmarkdown
  - Jupyter

📚 Supports : 
- [Slides](https://ifb-elixirfr.gitlab.io/training/fair-gaa/module-2-gestion-de-projet/)

## Descritpion Travaux Pratiques (TP)

⏱️ Durée : 3h

📑 Plan : 
- Organisation des données
- Sauvegarde du code (Git)
- Prise de notes / historique
- Jupyter
- Gitlab Pages

📚 Supports : 
- [Notebook Tutorial](https://ifb-elixirfr.gitlab.io/training/fair-gaa/module-2-gestion-de-projet/tutorial.html)
- [Git Tutorial](https://gitlab.com/ifb-elixirfr/training/notebooks/git-initiation)
